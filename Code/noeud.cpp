#include "noeud.hpp"

Noeud::Noeud(int v) :
  gauche(nullptr), //en l'absence d'enfants on a nullptr
  droite(nullptr),
  valeur(v)
{}

Noeud::~Noeud() {
  //le noeud est responsable de la destruction de ses enfants
  delete gauche ;
  delete droite ;
}
