#include "arbre.hpp"

#include <random>
#include <iostream>
#include <fstream>
#include <cassert>

int main() {
  Arbre a ;

  std::mt19937 alea ;

#ifndef DEBUG
  std::random_device vrai_alea ;
  alea.seed(vrai_alea()) ;
#endif

  std::uniform_int_distribution<int> nombre(0, 100) ;

  int size = 10 ;

  for(int i = 0; i < size; ++i) {
    a.inserer(nombre(alea)) ;
    assert(a.verifier_abr()) ;
  }
  a.afficher() ;

  return 0 ;
}
